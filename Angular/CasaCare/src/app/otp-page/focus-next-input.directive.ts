import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[focusNextInput]'
})
export class FocusNextInputDirective {
  @Input() index: number = 0;

  constructor(private el: ElementRef) {}

  @HostListener('input', ['$event'])
  onInputChange(event: Event) {
    const input = this.el.nativeElement;
    if (input.value.length === 1 && this.index < 5) {
      const nextInput = document.getElementById(`otp${this.index + 1}`);
      if (nextInput) {
        nextInput.focus();
      }
    }
  }
}