import { Component } from '@angular/core';
import { OtpService } from '../otp.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-otp-page',
  templateUrl: './otp-page.component.html',
  styleUrls: ['./otp-page.component.css']
})
export class OtpPageComponent {
  otp: string[] = ['', '', '', '', '', ''];
  errorMessage: string | null = null;

  constructor(private otpService: OtpService, private router: Router, private toastr: ToastrService) {}

  onOtpInput(event: Event, index: number) {
    const input = event.target as HTMLInputElement;
    const value = input.value;

    if (value.length === 1) {
      this.otp[index] = value;
      if (index < 5) {
        const nextInput = document.getElementById(`otp${index + 1}`) as HTMLInputElement;
        if (nextInput) {
          nextInput.focus();
        }
      }
    } else {
      this.otp[index] = '';
    }
  }

  onOtpSubmit() {
    const otpCode = this.otp.join('');
    const email = localStorage.getItem('email');
    if (!email) {
      this.errorMessage = 'Email not found. Please login again.';
      return;
    }

    if (otpCode.length !== 6) {
      this.errorMessage = 'OTP must be 6 digits.';
      return;
    }

    console.log('Retrieved email from localStorage:', email);
    console.log('OTP Code:', otpCode);

    this.otpService.verifyOtp(email, otpCode).subscribe(
      (response: any) => {
        console.log('OTP verification successful:', response);
        this.otpService.setUserLoggedIn();
        this.toastr.success('Login successful', 'Success');
        this.router.navigate(['/home']);
      },
      (error: HttpErrorResponse) => {
        console.error('Error verifying OTP:', error);
        if (error.status === 401) {
          this.errorMessage = 'Invalid OTP. Please try again.';
        } else {
          this.errorMessage = 'An unexpected error occurred. Please try again later.';
        }
      }
    );
  }
}
