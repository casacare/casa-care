import { Component } from '@angular/core';
import { OtpService } from '../otp.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent {

  constructor(private otpService: OtpService, private router: Router) {}

  ngOnInit() {
    this.otpService.setUserLoggedOut();
    alert("Successfully Logged Out!!!");
    this.router.navigate(['/login']);
  }
}
