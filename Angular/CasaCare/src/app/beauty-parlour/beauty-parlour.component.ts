import { Component, OnInit } from '@angular/core';
import { OtpService } from '../otp.service';

interface BeautyService {
  name: string;
  image: string;
  serviceType: string;
  description: string;
}

@Component({
  selector: 'app-beauty-parlour',
  templateUrl: './beauty-parlour.component.html',
  styleUrls: ['./beauty-parlour.component.css']
})
export class BeautyParlourComponent implements OnInit {
  selectedExpert: any = null;
  hairstyles: BeautyService[] = [];
  facials: BeautyService[] = [];
  massages: BeautyService[] = [];

  constructor(private beautyService: OtpService) {}

  ngOnInit() {
    this.fetchBeautyServices();
  }

  fetchBeautyServices() {
    this.beautyService.getBeautyServices().subscribe(data => {
      this.hairstyles = data.filter((service: BeautyService) => service.serviceType === 'HairService');
      this.facials = data.filter((service: BeautyService) => service.serviceType === 'FacialService');
      this.massages = data.filter((service: BeautyService) => service.serviceType === 'MassageService');
    });
  }
}
