import { Component, OnInit  } from '@angular/core';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { OtpService } from '../otp.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit{
  faShoppingCart = faShoppingCart;
  isLoggedIn: boolean = false;

  constructor(private otpService: OtpService, private router: Router) {}
  ngOnInit() {
    this.otpService.getUserLoginStatus().subscribe((status: boolean) => {
    this.isLoggedIn = status;
    });
  }

  logout() {
    this.otpService.setUserLoggedOut();
    this.router.navigate(['/login']);
  }
}
