import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  showDetails: boolean = false;

  constructor(private router: Router) {}

  onGetStarted(): void {
    this.showDetails = true;
  }

  navigateToHomeServices(): void {
    this.router.navigate(['/home-services']);
  }

  navigateToBeautyParlour(): void {
    this.router.navigate(['/beauty-parlour']);
  }
}
