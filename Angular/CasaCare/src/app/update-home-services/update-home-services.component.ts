import { Component, OnInit } from '@angular/core';
import { OtpService } from '../otp.service';
import { Router } from '@angular/router';

interface BeautyService {
  id: number;
  name: string;
  image: string;
  serviceType: string;
  description: string;
}

@Component({
  selector: 'app-update-home-services',
  templateUrl: './update-home-services.component.html',
  styleUrl: './update-home-services.component.css'
})
export class UpdateHomeServicesComponent implements OnInit{
  selectedExpert: any = null;
  hairstyles: BeautyService[] = [];
  facials: BeautyService[] = [];
  massages: BeautyService[] = [];

  constructor(private beautyService: OtpService, private router: Router) {}

  ngOnInit() {
    this.fetchBeautyServices();
  }

  fetchBeautyServices() {
    this.beautyService.getBeautyServices().subscribe(data => {
      this.hairstyles = data.filter((service: BeautyService) => service.serviceType === 'HairService');
      this.facials = data.filter((service: BeautyService) => service.serviceType === 'FacialService');
      this.massages = data.filter((service: BeautyService) => service.serviceType === 'MassageService');
    });
  }
  updateService(service: BeautyService) {
    this.router.navigate(['/update-form'], { state: { service } });
  }
  
  deleteService(serviceId: number) {
    this.beautyService.deleteBeautyService(serviceId).subscribe(response => {
      console.log('Service deleted:', response);
      this.fetchBeautyServices(); 
    }, error => {
      console.error('Error deleting service:', error);
    });
  }

  insertService() {
    this.router.navigate(['/insert-form']);
  }

}
