import { Component, OnInit } from '@angular/core';
import { OtpService } from './otp.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'] 
})
export class AppComponent {
  title = 'CasaCare';
  constructor(private otpService: OtpService) {}

  ngOnInit() {
    const isLoggedIn = !!localStorage.getItem('email');
    if (isLoggedIn) {
      this.otpService.setUserLoggedIn();
    }
  }
}