import { Component , OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OtpService } from '../otp.service';

@Component({
  selector: 'app-update-homeservice',
  templateUrl: './update-homeservice.component.html',
  styleUrl: './update-homeservice.component.css'
})
export class UpdateHomeserviceComponent implements OnInit {
  service: any;

  constructor(private router: Router, private otpService: OtpService) {
    const navigation = this.router.getCurrentNavigation();
    this.service = navigation?.extras?.state?.['service'];
  }

  ngOnInit(): void {
    if (!this.service) {
      this.router.navigate(['/update-home-service']);
    }
  }

  onSubmit() {
    
  }
}
