import { Component, OnInit } from '@angular/core';
import { OtpService } from '../otp.service';

interface HomeService {
  name: string;
  image: string;
  serviceType: string;
  description: string;
}

@Component({
  selector: 'app-home-service',
  templateUrl: './home-service.component.html',
  styleUrl: './home-service.component.css'
})
export class HomeServiceComponent implements OnInit{

  selectedExpert: any = null;
  homecleaning: HomeService[] = [];
  repair: HomeService[] = [];
  homeinstallation: HomeService[] = [];

  constructor(private homeService: OtpService) {}

  
  ngOnInit() {
    this.fetchHomeServices();
  }

  fetchHomeServices() {
    this.homeService.getHomeServices().subscribe(data => {
      this.homecleaning = data.filter((service: HomeService) => service.serviceType === 'HomeCleaning');
      this.repair = data.filter((service: HomeService) => service.serviceType === 'Repair');
      this.homeinstallation = data.filter((service: HomeService) => service.serviceType === 'HomeInstallation');
    });
  }
}


