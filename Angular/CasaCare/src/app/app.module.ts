import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { OtpPageComponent } from './otp-page/otp-page.component';
import { NavigationComponent } from './navigation/navigation.component';
import { HomeComponent } from './home/home.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactusComponent } from './contactus/contactus.component';
import { CartComponent } from './cart/cart.component';
import { FocusNextInputDirective } from './otp-page/focus-next-input.directive';
import { LogoutComponent } from './logout/logout.component';
import { BeautyParlourComponent } from './beauty-parlour/beauty-parlour.component';
import { FooterComponent } from './footer/footer.component';
import { OtpService } from './otp.service';
import { HomeServiceComponent } from './home-service/home-service.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { UpateBeautyServiceComponent } from './upate-beauty-service/upate-beauty-service.component';
import { UpdateFormComponent } from './update-form/update-form.component';
import { InsertFormComponent } from './insert-form/insert-form.component';
import { UpdateHomeServicesComponent } from './update-home-services/update-home-services.component';
import { UpdateHomeserviceComponent } from './update-homeservice/update-homeservice.component';
import { InserHomeserviceComponent } from './inser-homeservice/inser-homeservice.component';
import { InsertHomeserviceComponent } from './insert-homeservice/insert-homeservice.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    OtpPageComponent,
    NavigationComponent,
    HomeComponent,
    AboutusComponent,
    ContactusComponent,
    CartComponent,
    FocusNextInputDirective,
    LogoutComponent,
    BeautyParlourComponent,
    FooterComponent,
    HomeServiceComponent,
    UpateBeautyServiceComponent,
    UpdateFormComponent,
    InsertFormComponent,
    UpdateHomeServicesComponent,
    UpdateHomeserviceComponent,
    InserHomeserviceComponent,
    InsertHomeserviceComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModule,
    MatIconModule,
    MatToolbarModule,
    MatMenuModule,
    MatButtonModule,
    HttpClientModule,BrowserAnimationsModule, // required for toastr
    ToastrModule.forRoot({
      timeOut: 5000, // duration of the toast
      positionClass: 'toast-top-right', // position of the toast
      preventDuplicates: true, // prevent duplicate toasts
    }),
  ],
  providers: [OtpService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }