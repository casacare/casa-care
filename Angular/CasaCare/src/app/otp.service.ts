import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse,HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OtpService {
  private baseUrl = 'http://localhost:8085';
  private loginStatus = new BehaviorSubject<boolean>(this.checkInitialLoginStatus());

  constructor(private http: HttpClient) {}

  private checkInitialLoginStatus(): boolean {
    return !!localStorage.getItem('email'); // Check if email is stored in localStorage
  }

  setUserLoggedIn() {
    this.loginStatus.next(true);
    localStorage.setItem('isLoggedIn', 'true');
  }

  setUserLoggedOut() {
    this.loginStatus.next(false);
    localStorage.removeItem('isLoggedIn');
    localStorage.removeItem('email');
    localStorage.clear();
  }

  getUserLoginStatus(): Observable<boolean> {
    return this.loginStatus.asObservable();
  }


  sendOtpToEmail(email: string): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/register`, { email });
  }

  verifyOtp(email: string, otp: string): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    const body = { email, otp };
    return this.http.post<any>(`${this.baseUrl}/verify-otp`, body, { headers, responseType: 'json' });
  }

  getBeautyServices(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/api/beauty-services`);
  }
  
  getHomeServices(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/home-services`);
  }

  updateBeautyService(service: BeautyService): Observable<any> {
    return this.http.put<any>(`${this.baseUrl}/api/beauty-services/updateservice`, service);
  }

  deleteBeautyService(serviceId: number): Observable<any> {
    return this.http.delete<any>(`${this.baseUrl}/api/beauty-services/deleteserviceById/${serviceId}`).pipe(
      tap(
        response => console.log('Response:', response),
        error => console.error('Error deleting service:', error)
      )
    );
  }

  insertService(service: NewBeautyService): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/api/beauty-services/addService`, service);
  }

}

interface BeautyService {
  id: number;
  name: string;
  image: string;
  serviceType: string;
  description: string;
}

export interface NewBeautyService {
  name: string;
  image: string;
  serviceType: string;
  description: string;
}