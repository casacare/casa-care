import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OtpPageComponent } from './otp-page/otp-page.component';
import { HomeComponent } from './home/home.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactusComponent } from './contactus/contactus.component';
import { CartComponent } from './cart/cart.component';
import { LoginComponent } from './login/login.component';
import { BeautyParlourComponent } from './beauty-parlour/beauty-parlour.component';
import { HomeServiceComponent } from './home-service/home-service.component';
import {UpateBeautyServiceComponent} from './upate-beauty-service/upate-beauty-service.component';
import { UpdateFormComponent } from './update-form/update-form.component';
import {InsertFormComponent} from './insert-form/insert-form.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: 'aboutus', component: AboutusComponent },
  { path: 'contactus', component: ContactusComponent },
  { path: 'cart', component: CartComponent },
  { path: 'otp', component: OtpPageComponent },
  { path: 'beauty-parlour', component: BeautyParlourComponent },
  { path: 'home-services', component: HomeServiceComponent },
  { path: 'update-beauty-services', component: UpateBeautyServiceComponent },
  { path: 'update-form', component: UpdateFormComponent },
  { path: 'insert-form', component: InsertFormComponent },
  { path: '**', redirectTo: '/home' } 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }