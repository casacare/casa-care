import { Component } from '@angular/core';
import { OtpService } from '../otp.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http'; 
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  customer: { emailId: string } = { emailId: '' };
  error: string | undefined; 

  constructor(private otpService: OtpService, private router: Router, private toastr: ToastrService) { }

  sendOtp() {
    console.log('Sending OTP...'); 
    this.otpService.sendOtpToEmail(this.customer.emailId).subscribe(
      () => {
        console.log('OTP Sent successfully!'); 
        localStorage.setItem('email', this.customer.emailId);
        console.log('Email stored in localStorage:', this.customer.emailId);
        this.toastr.success('OTP sent successfully!', 'Success');
        setTimeout(() => {
          this.router.navigate(['/otp']);
        }, 1000); 
      },
      (errorResponse: HttpErrorResponse) => {
        console.log('Error sending OTP:', errorResponse); 
        if (errorResponse.error && typeof errorResponse.error === 'object' && errorResponse.error.text) {
          this.error = errorResponse.error.text; 
        } else {
          this.error = 'Error sending OTP'; 
        }
      }
    );
  }
}

