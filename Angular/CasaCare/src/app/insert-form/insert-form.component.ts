import { Component } from '@angular/core';
import { OtpService, NewBeautyService } from '../otp.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-insert-form',
  templateUrl: './insert-form.component.html',
  styleUrls: ['./insert-form.component.css']
})
export class InsertFormComponent {

  constructor(private beautyService: OtpService, private router: Router) {}

  onSubmit(form: NgForm) {
    const formData: NewBeautyService = form.value;
    this.beautyService.insertService(formData).subscribe(
      response => {
        console.log('Service inserted:', response);
        this.router.navigate(['/update-beauty-services']);
      },
      error => {
        console.error('Error inserting service:', error);
      }
    );
  }
}
