import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OtpService } from '../otp.service';

@Component({
  selector: 'app-update-form',
  templateUrl: './update-form.component.html',
  styleUrls: ['./update-form.component.css']
})
export class UpdateFormComponent implements OnInit {
  service: any;

  constructor(private router: Router, private otpService: OtpService) {
    const navigation = this.router.getCurrentNavigation();
    this.service = navigation?.extras?.state?.['service'];
  }

  ngOnInit(): void {
    if (!this.service) {
      this.router.navigate(['/update-beauty-service']);
    }
  }

  onSubmit() {
    this.otpService.updateBeautyService(this.service).subscribe(
      response => {
        console.log('Service details updated:', response);
        this.router.navigate(['/update-beauty-service']);
      },
      error => {
        console.error('Error updating service details:', error);
      }
    );
  }
}
