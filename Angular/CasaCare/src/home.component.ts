// import { Component } from '@angular/core';

// @Component({
//   selector: 'app-home',
//   templateUrl: './home.component.html',
//   styleUrl: './home.component.css'
// })
// export class HomeComponent {

// }
import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  showDetails: boolean = false; // Flag to toggle displaying details section

  constructor() { }

  // Function to handle the click event of the "Get Started" button
  onGetStarted(): void {
    // Toggle the value of the showDetails flag to show/hide the details section
    this.showDetails = !this.showDetails;
  }
}
