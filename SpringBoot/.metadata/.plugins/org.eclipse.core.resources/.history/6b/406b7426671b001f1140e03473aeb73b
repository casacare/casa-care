package com.casacare.model;
import javax.persistence.*;

@Entity
@Table(name = "beauty_service")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "service_type", discriminatorType = DiscriminatorType.STRING)
public abstract class BeautyService {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private String name;
    private String image;
    private String description;

    public BeautyService() {}

    public BeautyService(String name, String image, String description) {
        this.name = name;
        this.image = image;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public abstract String getServiceType();
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

@Entity
@DiscriminatorValue("FacialService")
class FacialService extends BeautyService {
    public FacialService() {}

    public FacialService(String name, String image, String description) {
        super(name, image, description);
    }
    
    @Override
    public String getServiceType() {
        return "FacialService";
    }
}

@Entity
@DiscriminatorValue("HairService")
class HairService extends BeautyService {
    public HairService() {}

    public HairService(String name, String image, String description) {
        super(name, image, description);
    }
    
    @Override
    public String getServiceType() {
        return "HairService";
    }
}

@Entity
@DiscriminatorValue("MassageService")
class MassageService extends BeautyService {
    public MassageService() {}

    public MassageService(String name, String image, String description) {
        super(name, image, description);
    }
    
    @Override
    public String getServiceType() {
        return "MassageService";
    }
}
