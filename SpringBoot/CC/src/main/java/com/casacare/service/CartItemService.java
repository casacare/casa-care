package com.casacare.service;

import com.casacare.model.CartItem;
import com.casacare.dao.CartItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CartItemService {
    private final CartItemRepository repository;

    @Autowired
    public CartItemService(CartItemRepository repository) {
        this.repository = repository;
    }

    public List<CartItem> findAll() {
        List<CartItem> cartItems = repository.findAll();
        cartItems.forEach(cartItem -> {
            if (cartItem.getService() != null) {
                cartItem.setServiceImage(cartItem.getService().getImage());
            }
        });
        return cartItems;
    }

    public List<CartItem> findByUserId(Long userId) {
        return repository.findByUserId(userId);
    }

    public Optional<CartItem> findById(Long id) {
        return repository.findById(id);
    }

    public CartItem save(CartItem cartItem) {
        return repository.save(cartItem);
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }
}