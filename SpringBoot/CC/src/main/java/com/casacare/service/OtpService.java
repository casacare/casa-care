package com.casacare.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class OtpService {

    @Autowired
    private JavaMailSender javaMailSender;

    private Map<String, String> otpMap = new HashMap<>();

    private static final Logger logger = LoggerFactory.getLogger(OtpService.class);

    public String generateOtp() {
        Random random = new Random();
        int otp = 100000 + random.nextInt(900000); 
        return String.valueOf(otp);
    }

    public void sendOtp(String email, String otp) {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(email);
            message.setSubject("Your OTP");
            message.setText("Your OTP is: " + otp);
            javaMailSender.send(message);
            logger.info("OTP sent to email: {}", email);
        } catch (Exception e) {
            logger.error("Failed to send OTP email", e);
            throw new RuntimeException("Failed to send OTP email", e);
        }
    }

    public void storeOtp(String email, String otp) {
        otpMap.put(email, otp);
        logger.info("OTP stored for email: {}", email);
    }

    public boolean verifyOtp(String email, String otp) {
        String storedOtp = otpMap.get(email);
        boolean isValid = storedOtp != null && storedOtp.equals(otp);
        logger.info("OTP verification for email: {} - {}", email, isValid ? "successful" : "failed");
        return isValid;
    }

    public void sendWelcomeEmail(String email) {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(email);
            message.setSubject("Welcome to CasaCare");
            message.setText("Welcome to CasaCare! Enjoy our services. You are now successfully registered and logged in.");
            javaMailSender.send(message);
            logger.info("Welcome email sent to: {}", email);
        } catch (Exception e) {
            logger.error("Failed to send welcome email", e);
            throw new RuntimeException("Failed to send welcome email", e);
        }
    }
}

