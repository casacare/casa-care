package com.casacare.service;

import com.casacare.model.BeautyService;
import com.casacare.dao.BeautyServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BeautyServiceService {
    private final BeautyServiceRepository repository;

    @Autowired
    public BeautyServiceService(BeautyServiceRepository repository) {
        this.repository = repository;
    }

    public List<BeautyService> findAll() {
        return repository.findAll();
    }

    public Optional<BeautyService> findById(Long id) {
        return repository.findById(id);
    }

    public BeautyService save(BeautyService beautyService) {
        return repository.save(beautyService);
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }
}
