package com.casacare.service;

import com.casacare.model.ServiceCategory;
import com.casacare.dao.ServiceCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServiceCategoryService {
    private final ServiceCategoryRepository repository;

    @Autowired
    public ServiceCategoryService(ServiceCategoryRepository repository) {
        this.repository = repository;
    }

    public List<ServiceCategory> findAll() {
        return repository.findAll();
    }

    public Optional<ServiceCategory> findById(Long id) {
        return repository.findById(id);
    }

    public ServiceCategory save(ServiceCategory category) {
        return repository.save(category);
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }
}
