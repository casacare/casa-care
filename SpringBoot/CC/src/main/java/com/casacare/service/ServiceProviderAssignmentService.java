package com.casacare.service;

import com.casacare.model.ServiceProviderAssignment;
import com.casacare.dao.ServiceProviderAssignmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServiceProviderAssignmentService {
    private final ServiceProviderAssignmentRepository repository;

    @Autowired
    public ServiceProviderAssignmentService(ServiceProviderAssignmentRepository repository) {
        this.repository = repository;
    }

    public List<ServiceProviderAssignment> findAll() {
        return repository.findAll();
    }

    public Optional<ServiceProviderAssignment> findById(Long id) {
        return repository.findById(id);
    }

    public ServiceProviderAssignment save(ServiceProviderAssignment assignment) {
        return repository.save(assignment);
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }
}
