package com.casacare.model;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "service_type", discriminatorType = DiscriminatorType.STRING)
public abstract class BeautyService {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String image;

    public BeautyService() {}

    public BeautyService(String name, String image) {
        this.name = name;
        this.image = image;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public abstract String getServiceType();
}

@Entity
@DiscriminatorValue("Facial")
class Facial extends BeautyService {
    public Facial() {}

    public Facial(String name, String image) {
        super(name, image);
    }

    @Override
    public String getServiceType() {
        return "Facial";
    }
}

@Entity
@DiscriminatorValue("Hairstyle")
class Hairstyle extends BeautyService {
    public Hairstyle() {}

    public Hairstyle(String name, String image) {
        super(name, image);
    }

    @Override
    public String getServiceType() {
        return "Hairstyle";
    }
}

@Entity
@DiscriminatorValue("Massage")
class Massage extends BeautyService {
    public Massage() {}

    public Massage(String name, String image) {
        super(name, image);
    }

    @Override
    public String getServiceType() {
        return "Massage";
    }
}
