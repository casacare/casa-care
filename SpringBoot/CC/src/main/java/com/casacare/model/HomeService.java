package com.casacare.model;

import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
	    use = JsonTypeInfo.Id.NAME,
	    include = JsonTypeInfo.As.PROPERTY,
	    property = "serviceType"
	)
	@JsonSubTypes({
	    @JsonSubTypes.Type(value = HomeCleaningService.class, name = "HomeCleaning"),
	    @JsonSubTypes.Type(value = RepairService.class, name = "Repair"),
	    @JsonSubTypes.Type(value = HomeInstallationService.class, name = "HomeInstallation")
	})
@Entity
@Table(name = "home_service")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "service_type", discriminatorType = DiscriminatorType.STRING)
public abstract class HomeService {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private String name;
    private String image;
    private String description;

    public HomeService() {}

    public HomeService(String name, String image, String description) {
        this.name = name;
        this.image = image;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public abstract String getServiceType();
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

@Entity
@DiscriminatorValue("HomeCleaning")
class HomeCleaningService extends HomeService {
    public HomeCleaningService() {}

    public HomeCleaningService(String name, String image, String description) {
        super(name, image, description);
    }
    
    @Override
    public String getServiceType() {
        return "HomeCleaning";
    }
}

@Entity
@DiscriminatorValue("Repair")
class RepairService extends HomeService {
    public RepairService() {}

    public RepairService(String name, String image, String description) {
        super(name, image, description);
    }
    
    @Override
    public String getServiceType() {
        return "Repair";
    }
}

@Entity
@DiscriminatorValue("HomeInstallation")
class HomeInstallationService extends HomeService {
    public HomeInstallationService() {}

    public HomeInstallationService(String name, String image, String description) {
        super(name, image, description);
    }
    
    @Override
    public String getServiceType() {
        return "HomeInstallation";
    }
}
