package com.casacare.controller;

import com.casacare.model.ServiceCategory;
import com.casacare.service.ServiceCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/service-categories")
public class ServiceCategoryController {
    private final ServiceCategoryService service;

    @Autowired
    public ServiceCategoryController(ServiceCategoryService service) {
        this.service = service;
    }

    @GetMapping
    public List<ServiceCategory> getAllServiceCategories() {
        return service.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<ServiceCategory> getServiceCategoryById(@PathVariable Long id) {
        Optional<ServiceCategory> optionalServiceCategory = service.findById(id);
        return optionalServiceCategory.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public ServiceCategory createServiceCategory(@RequestBody ServiceCategory serviceCategory) {
        return service.save(serviceCategory);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ServiceCategory> updateServiceCategory(@PathVariable Long id, @RequestBody ServiceCategory updatedServiceCategory) {
        Optional<ServiceCategory> optionalServiceCategory = service.findById(id);
        if (optionalServiceCategory.isPresent()) {
            updatedServiceCategory.setId(id);
            return ResponseEntity.ok(service.save(updatedServiceCategory));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteServiceCategory(@PathVariable Long id) {
        if (service.findById(id).isPresent()) {
            service.deleteById(id);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
