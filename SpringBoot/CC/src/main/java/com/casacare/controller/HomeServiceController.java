package com.casacare.controller;
import com.casacare.model.BeautyService;
import com.casacare.model.HomeService;
import com.casacare.service.HomeServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/home-services")
public class HomeServiceController {
    @Autowired
    private HomeServiceService service;

    @GetMapping
    public List<HomeService> getAllServices() {
        return service.getAllServices();
    }
    
    @PutMapping("/updateservice")
    public HomeService updateService(@RequestBody HomeService updatedService) {
        return service.updateService(updatedService);
    }
    
    @DeleteMapping("/deleteserviceById/{id}")
	public String deleteServiceById(@PathVariable("id") Long servId) {
		service.deleteServiceById(servId);
		return "Service Deleted Successfully!!";
	}
    
    @PostMapping("/addService")
	public HomeService addService(@RequestBody HomeService serv) {
		return service.addService(serv);
	}
}