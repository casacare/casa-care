// CartItemController.java
package com.casacare.controller;

import com.casacare.model.CartItem;
import com.casacare.service.CartItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/cart-items")
public class CartItemController {
    private final CartItemService service;

    @Autowired
    public CartItemController(CartItemService service) {
        this.service = service;
    }

    @GetMapping
    public List<CartItem> getAllCartItems() {
        return service.findAll();
    }

    @GetMapping("/{userId}")
    public List<CartItem> getCartItemsByUser(@PathVariable Long userId) {
        return service.findByUserId(userId);
    }

    @PostMapping
    public CartItem createCartItem(@RequestBody CartItem cartItem) {
        return service.save(cartItem);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CartItem> updateCartItem(@PathVariable Long id, @RequestBody CartItem updatedCartItem) {
        Optional<CartItem> optionalCartItem = service.findById(id);
        if (optionalCartItem.isPresent()) {
            updatedCartItem.setId(id);
            return ResponseEntity.ok(service.save(updatedCartItem));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCartItem(@PathVariable Long id) {
        if (service.findById(id).isPresent()) {
            service.deleteById(id);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
