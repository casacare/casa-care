package com.casacare.controller;

import com.casacare.model.BeautyService;
import com.casacare.service.BeautyServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/beauty-services")
public class BeautyServiceController {
    private final BeautyServiceService service;

    @Autowired
    public BeautyServiceController(BeautyServiceService service) {
        this.service = service;
    }

    @GetMapping
    public List<BeautyService> getAllBeautyServices() {
        return service.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<BeautyService> getBeautyServiceById(@PathVariable Long id) {
        Optional<BeautyService> optionalBeautyService = service.findById(id);
        return optionalBeautyService.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public BeautyService createBeautyService(@RequestBody BeautyService beautyService) {
        return service.save(beautyService);
    }

    @PutMapping("/{id}")
    public ResponseEntity<BeautyService> updateBeautyService(@PathVariable Long id, @RequestBody BeautyService updatedBeautyService) {
        Optional<BeautyService> optionalBeautyService = service.findById(id);
        if (optionalBeautyService.isPresent()) {
            updatedBeautyService.setId(id);
            return ResponseEntity.ok(service.save(updatedBeautyService));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBeautyService(@PathVariable Long id) {
        if (service.findById(id).isPresent()) {
            service.deleteById(id);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
