package com.casacare.controller;

import com.casacare.model.ServiceProviderAssignment;
import com.casacare.service.ServiceProviderAssignmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/service-provider-assignments")
public class ServiceProviderAssignmentController {
    private final ServiceProviderAssignmentService service;

    @Autowired
    public ServiceProviderAssignmentController(ServiceProviderAssignmentService service) {
        this.service = service;
    }

    @GetMapping
    public List<ServiceProviderAssignment> getAllServiceProviderAssignments() {
        return service.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<ServiceProviderAssignment> getServiceProviderAssignmentById(@PathVariable Long id) {
        Optional<ServiceProviderAssignment> optionalServiceProviderAssignment = service.findById(id);
        return optionalServiceProviderAssignment.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public ServiceProviderAssignment createServiceProviderAssignment(@RequestBody ServiceProviderAssignment serviceProviderAssignment) {
        return service.save(serviceProviderAssignment);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ServiceProviderAssignment> updateServiceProviderAssignment(@PathVariable Long id, @RequestBody ServiceProviderAssignment updatedServiceProviderAssignment) {
        Optional<ServiceProviderAssignment> optionalServiceProviderAssignment = service.findById(id);
        if (optionalServiceProviderAssignment.isPresent()) {
            updatedServiceProviderAssignment.setId(id);
            return ResponseEntity.ok(service.save(updatedServiceProviderAssignment));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteServiceProviderAssignment(@PathVariable Long id) {
        if (service.findById(id).isPresent()) {
            service.deleteById(id);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
