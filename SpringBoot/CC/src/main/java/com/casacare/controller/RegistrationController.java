package com.casacare.controller;

import com.casacare.model.User;
import com.casacare.service.OtpService;
import com.casacare.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/auth")
public class RegistrationController {

    @Autowired
    private UserService userService;

    @Autowired
    private OtpService otpService;

    @PostMapping("/register")
    public ResponseEntity<Map<String, String>> register(@RequestBody RegistrationRequest registrationRequest) {
        String email = registrationRequest.getEmail();
        System.out.println("Register request received for email: " + email);

        // Check if user already exists
        if (userService.userExists(email)) {
            // Generate OTP
            String otp = otpService.generateOtp();

            // Send OTP to the provided email
            otpService.sendOtp(email, otp);
            
            // Store OTP
            otpService.storeOtp(email, otp);

            Map<String, String> response = new HashMap<>();
            response.put("message", "OTP sent successfully!");
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } else {
            Map<String, String> response = new HashMap<>();
            response.put("message", "User already registered. Please proceed to login.");
            return ResponseEntity.status(HttpStatus.OK).body(response);
        }
    }
    
    @PostMapping("/verify-otp")
    public ResponseEntity<Map<String, String>> verifyOtp(@RequestBody Map<String, String> request) {
        String email = request.get("email");
        String otp = request.get("otp");

        Map<String, String> response = new HashMap<>();
        if (otpService.verifyOtp(email, otp)) {
            User user = userService.findByEmail(email);
            if (user == null) {
                user = new User();
                user.setEmail(email);
                userService.saveUser(user);
            }

            otpService.sendWelcomeEmail(email);

            response.put("message", "OTP verified successfully and user registered!");
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } else {
            response.put("message", "OTP verification failed!");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
        }
    }

    @PostMapping("/login")
    public ResponseEntity<Map<String, String>> login(@RequestBody User user) {
        String email = user.getEmail();
        User existingUser = userService.findByEmail(email);
        Map<String, String> response = new HashMap<>();
        if (existingUser != null) {
            response.put("message", "User logged in successfully!");
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } else {
            response.put("message", "User does not exist. Please register first.");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        }
    }
    
}



