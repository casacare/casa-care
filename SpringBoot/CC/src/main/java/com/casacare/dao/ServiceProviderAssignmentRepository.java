package com.casacare.dao;

import com.casacare.model.ServiceProviderAssignment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceProviderAssignmentRepository extends JpaRepository<ServiceProviderAssignment, Long> {
}
