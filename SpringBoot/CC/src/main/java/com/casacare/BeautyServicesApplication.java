package com.casacare;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeautyServicesApplication {
    public static void main(String[] args) {
        SpringApplication.run(BeautyServicesApplication.class, args);
    }
}